package cn.ydxiaoshuai.common.factory;

import cn.ydxiaoshuai.common.sdkpro.AipFacePro;
import cn.ydxiaoshuai.common.sdkpro.AipImageClassifyPro;
import com.baidu.aip.bodyanalysis.AipBodyAnalysis;
import com.baidu.aip.easydl.AipEasyDL;
import com.baidu.aip.ocr.AipOcr;
import org.springframework.stereotype.Component;

/**
 * @Description 加载各个模块对象
 * @author 小帅丶
 * @className BDFactory
 * @Date 2020年5月14日17:47:05
 **/
@Component
public class BDFactory {

    private static AipFacePro aipFacePro;
    private static AipBodyAnalysis aipBodyAnalysis;
    private static AipImageClassifyPro aipImageClassifyPro;
    private static AipEasyDL aipEasyDL;
    private static AipOcr  aipOcr;
    /** 文字识别 非必须*/
    private static String appid = "";
    private static String apikey = "";
    private static String secretkey = "";
    /** 人脸识别(斑痘痣等) */
    private static String appid_face = "";
    private static String apikey_face = "";
    private static String secretkey_face = "";
    /** 人体分析 非必须*/
    private static String appid_body = "";
    private static String apikey_body = "";
    private static String secretkey_body = "";
    /** 图像识别(菜品、LOGO、车型等) */
    private static String appid_icr = "";
    private static String apikey_icr = "";
    private static String secretkey_icr = "";
    /** EasyDL 非必须*/
    private static String appid_ezdl = "";
    private static String apikey_ezdl = "";
    private static String secretkey_ezdl = "";

    public static AipOcr getAipOcr(){
        if(aipOcr==null){
            synchronized (AipFacePro.class) {
                if(aipOcr==null){
                    aipOcr = new AipOcr(appid, apikey, secretkey);
                }
            }
        }
        return aipOcr;
    }

    public static AipFacePro getAipFacePro(){
        if(aipFacePro==null){
            synchronized (AipFacePro.class) {
                if(aipFacePro==null){
                    aipFacePro = new AipFacePro(appid_face, apikey_face, secretkey_face);
                }
            }
        }
        return aipFacePro;
    }

    public static AipBodyAnalysis getAipBodyAnalysis(){
        if(aipBodyAnalysis==null){
            synchronized (AipBodyAnalysis.class) {
                if(aipBodyAnalysis==null){
                    aipBodyAnalysis = new AipBodyAnalysis(appid_body, apikey_body, secretkey_body);
                }
            }
        }
        return aipBodyAnalysis;
    }

    public static AipImageClassifyPro getAipImageClassifyPro(){
        if(aipImageClassifyPro==null){
            synchronized (AipImageClassifyPro.class) {
                if(aipImageClassifyPro==null){
                    aipImageClassifyPro = new AipImageClassifyPro(appid_icr, apikey_icr, secretkey_icr);
                }
            }
        }
        return aipImageClassifyPro;
    }
    public static AipEasyDL getAipEasyDL(){
        if(aipEasyDL==null){
            synchronized (AipEasyDL.class) {
                if(aipEasyDL==null){
                    aipEasyDL = new AipEasyDL(appid_ezdl, apikey_ezdl, secretkey_ezdl);
                }
            }
        }
        return aipEasyDL;
    }
}
