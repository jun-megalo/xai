package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 小帅丶
 * @className MedicalBeautySkinColorResponse
 * @Description 肤色检测返回的对象
 * @Date 2020/4/14-14:10
 **/
@NoArgsConstructor
@Data
public class MedicalBeautySkinColorResponse {

    private int error_code;
    private String error_msg;
    private long log_id;
    private int timestamp;
    private int cached;
    private ResultBean result;

    @NoArgsConstructor
    @Data
    public static class ResultBean {
        private int face_num;
        private List<FaceListBean> face_list;

        @NoArgsConstructor
        @Data
        public static class FaceListBean {
            private String face_token;
            private LocationBean location;
            private SkinBean skin;

            @NoArgsConstructor
            @Data
            public static class LocationBean {
                private double left;
                private double top;
                private int width;
                private int height;
                private int degree;
            }

            @NoArgsConstructor
            @Data
            public static class SkinBean {
                private int color;
            }
        }
    }
}
