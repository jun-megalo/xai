package cn.ydxiaoshuai.common.api.vo.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 小帅丶
 * @className FaceDetectBean
 * @Description 人脸检测接口返回页面的对象
 * @Date 2020年10月10日13:44:07
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FaceDetectBean extends BaseBean {
    /** 具体返回的内容 */
    private FaceDetectResponse.ResultBean data;
    /** 具体返回的内容 */
    private Data data_flower;
    @lombok.Data
    public static class Data{
        private String image_base64;
        private Integer acne_count=0;
        private Integer speckle_count=0;
        private Integer mole_count=0;
    }
    public FaceDetectBean success(String msg, String msg_zh) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        return this;
    }
    public FaceDetectBean success(String msg, String msg_zh, Data data_flower) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        this.data_flower = data_flower;
        return this;
    }
    public FaceDetectBean success(String msg, String msg_zh, FaceDetectResponse.ResultBean data) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        this.data = data;
        return this;
    }
    public FaceDetectBean fail(String msg, String msg_zh, Integer code) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = code;
        return this;
    }
    public FaceDetectBean error(String msg, String msg_zh) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 500;
        return this;
    }
}
