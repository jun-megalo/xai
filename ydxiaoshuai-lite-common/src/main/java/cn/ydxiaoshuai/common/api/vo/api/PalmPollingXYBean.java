package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 小帅丶
 * @className PalmPollingXYBean
 * @Description //TODO
 * @Date 2020/5/8-10:34
 **/
@NoArgsConstructor
@Data
public class PalmPollingXYBean {

    private String msg;
    private int code;
    private DataBean data;

    @NoArgsConstructor
    @Data
    public static class DataBean {

        private boolean isLeft;
        private HandBean hand;
        private PalmBean palm;
        private FingersBean fingers;
        private String imgUrl;
        private LinesBean lines;

        @NoArgsConstructor
        @Data
        public static class HandBean {
            private List<Integer> x;
            private List<Integer> y;
        }

        @NoArgsConstructor
        @Data
        public static class PalmBean {

            private double score;
            private List<Integer> x;
            private List<Integer> y;
        }

        @NoArgsConstructor
        @Data
        public static class FingersBean {

            private MiddleBean middle;
            private RingBean ring;
            private IndexBean index;
            private LittleBean little;

            @NoArgsConstructor
            @Data
            public static class MiddleBean {

                private double score;
                private List<Integer> x;
                private List<Integer> y;
            }

            @NoArgsConstructor
            @Data
            public static class RingBean {

                private double score;
                private List<Integer> x;
                private List<Integer> y;
            }

            @NoArgsConstructor
            @Data
            public static class IndexBean {

                private double score;
                private List<Integer> x;
                private List<Integer> y;
            }

            @NoArgsConstructor
            @Data
            public static class LittleBean {

                private double score;
                private List<Integer> x;
                private List<Integer> y;
            }
        }

        @NoArgsConstructor
        @Data
        public static class LinesBean {

            private LLBean LL;
            private WLBean WL;
            private ALBean AL;

            @NoArgsConstructor
            @Data
            public static class LLBean {
                private List<Integer> x;
                private List<Integer> y;
            }

            @NoArgsConstructor
            @Data
            public static class WLBean {
                private List<Integer> x;
                private List<Integer> y;
            }

            @NoArgsConstructor
            @Data
            public static class ALBean {
                private List<Integer> x;
                private List<Integer> y;
            }
        }
    }
}
