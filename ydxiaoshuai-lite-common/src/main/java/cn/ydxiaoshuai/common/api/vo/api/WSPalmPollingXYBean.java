package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 小帅丶
 * @className WSPalmPollingXYBean
 * @Description 微算手相
 * @Date 2020/5/8-10:34
 **/
@NoArgsConstructor
@Data
public class WSPalmPollingXYBean {
    private LinesBean lines;
        @NoArgsConstructor
        @Data
        public static class LinesBean {
            //生命线
            private LLBean LL;
            //智慧线
            private WLBean WL;
            //感情线
            private ALBean AL;
            //事业线
            private CLBean CL;

            @NoArgsConstructor
            @Data
            public static class LLBean {
                private List<Integer> x;
                private List<Integer> y;
            }

            @NoArgsConstructor
            @Data
            public static class WLBean {
                private List<Integer> x;
                private List<Integer> y;
            }

            @NoArgsConstructor
            @Data
            public static class ALBean {
                private List<Integer> x;
                private List<Integer> y;
            }

            @NoArgsConstructor
            @Data
            public static class CLBean {
                private List<Integer> x;
                private List<Integer> y;
            }
        }
}
