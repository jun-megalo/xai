package cn.ydxiaoshuai.common.api.vo.palm;

import lombok.Data;

/**
 * @Description 获取分析数据ID
 * @author 小帅丶
 * @className PalmUploadImgUrlBean
 * @Date 2020/1/3-10:43
 **/
@Data
public class PalmUploadImgUrlBean {
    /**
     * code : 0
     * msg : success
     * data : {"id":"7d3c4b748cbf42a3b4a5bf0ecd941565202001"}
     */
    /** 应答码 */
    private int code;
    /** 应答信息 */
    private String msg;
    /** 具体数据 */
    private DataBean data;

    @Data
    public static class DataBean {
        /**
         * id : 7d3c4b748cbf42a3b4a5bf0ecd941565202001
         */
        /** 图片数据ID */
        private String id;

    }
}
