package cn.ydxiaoshuai.modules.quartz.job;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.http.HttpUtil;
import cn.ydxiaoshuai.common.api.vo.api.TouTiaoAccessTokenBean;
import cn.ydxiaoshuai.common.util.RedisUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import cn.ydxiaoshuai.common.util.oConvertUtils;

import java.util.Date;

/**
 * @author 小帅丶
 * @className ToutiaoJob
 * @Description 头条job
 * @Date 2020/9/11-11:53
 **/
@Slf4j
@Component
public class ToutiaoJob implements Job {
    @Autowired
    private RedisUtil redisUtil;
    private static String AT_TOUTIAO_URL = "";
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        getAccessTokenTouTiao();
    }
    /**
     * @Author 小帅丶
     * @Description 2小时执行一次获取token
     * @Date  2020/6/17 16:40
     * @return void
     **/
    public void getAccessTokenTouTiao(){
        if(oConvertUtils.isEmpty(AT_TOUTIAO_URL)){
            log.info("头条获取AccessToken获取失败，请对AT_TOUTIAO_URL进行赋值，非必须");
        }else{
            String result = HttpUtil.get(AT_TOUTIAO_URL);
            log.info("getAccessTokenTouTiao执行时间{}", DateUtil.format(new Date(), DatePattern.NORM_DATETIME_PATTERN));
            log.info("头条获取AccessToken返回的内容:"+result);
            TouTiaoAccessTokenBean bean = JSON.parseObject(result,TouTiaoAccessTokenBean.class);
            if(null!=bean.getAccess_token()){
                redisUtil.set("TTAT", bean.getAccess_token());
            }
        }
    }
}
