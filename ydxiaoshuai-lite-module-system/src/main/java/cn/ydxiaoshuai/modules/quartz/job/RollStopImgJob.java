package cn.ydxiaoshuai.modules.quartz.job;

import cn.ydxiaoshuai.modules.entity.LiteConfigSlide;
import cn.ydxiaoshuai.modules.service.ILiteConfigSlideService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author 小帅丶
 * @className RollImgJob
 * @Description 轮播图停用
 * @Date 2020年10月9日09:34:15
 **/
@Slf4j
public class RollStopImgJob implements Job {
    /**
     * 若参数变量名修改 QuartzJobController中也需对应修改
     */
    private String parameter;

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }
    @Autowired
    private ILiteConfigSlideService liteConfigSlideService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        rollingImg(parameter);
    }
    /**
     * @Author 小帅丶
     * @Description 启动轮播图
     * @Date  2020/10/9 9:51
     * @param parameter 多个英文逗号隔开
     * @return void
     **/
    private void rollingImg(String parameter) {
        log.info("关闭轮播ids:{}",parameter);
        long startTime = System.currentTimeMillis();
        String ids[] = parameter.split(",");
        for (int i = 0; i < ids.length; i++) {
            LiteConfigSlide liteConfigSlideDB = liteConfigSlideService.getById(ids[i]);
            if(null!=liteConfigSlideDB){
                liteConfigSlideDB.setIsShow(1);
                liteConfigSlideService.updateById(liteConfigSlideDB);
            }
        }
        long endTime = System.currentTimeMillis();
        log.info("关闭轮播耗时:{}",(endTime-startTime));
    }


}
