package cn.ydxiaoshuai.modules.system.service.impl;

import cn.ydxiaoshuai.modules.system.entity.SysUserAgent;
import cn.ydxiaoshuai.modules.system.mapper.SysUserAgentMapper;
import cn.ydxiaoshuai.modules.system.service.ISysUserAgentService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 用户代理人设置
 * @Author: 小帅丶
 * @Date:  2019-04-17
 * @Version: V1.0
 */
@Service
public class SysUserAgentServiceImpl extends ServiceImpl<SysUserAgentMapper, SysUserAgent> implements ISysUserAgentService {

}
