package cn.ydxiaoshuai.modules.oss.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.ydxiaoshuai.modules.oss.entity.OSSFile;

public interface OSSFileMapper extends BaseMapper<OSSFile> {

}
