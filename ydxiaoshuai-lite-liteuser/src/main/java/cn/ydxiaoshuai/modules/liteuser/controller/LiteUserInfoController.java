package cn.ydxiaoshuai.modules.liteuser.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.ydxiaoshuai.common.api.vo.Result;
import cn.ydxiaoshuai.common.aspect.annotation.AutoLog;
import cn.ydxiaoshuai.common.system.base.controller.JeecgController;
import cn.ydxiaoshuai.common.system.query.QueryGenerator;
import cn.ydxiaoshuai.modules.liteuser.entity.LiteUserInfo;
import cn.ydxiaoshuai.modules.liteuser.service.ILiteUserInfoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

 /**
 * @Description: 微信用户信息表
 * @Author: 小帅丶
 * @Date:   2020-05-14
 * @Version: V1.0
 */
@Slf4j
@Api(tags="微信用户信息表")
@RestController
@RequestMapping("/liteuser/liteUserInfo")
public class LiteUserInfoController extends JeecgController<LiteUserInfo, ILiteUserInfoService> {
	@Autowired
	private ILiteUserInfoService liteUserInfoService;
	
	/**
	 * 分页列表查询
	 *
	 * @param liteUserInfo
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "微信用户信息表-分页列表查询")
	@ApiOperation(value="微信用户信息表-分页列表查询", notes="微信用户信息表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(LiteUserInfo liteUserInfo,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<LiteUserInfo> queryWrapper = QueryGenerator.initQueryWrapper(liteUserInfo, req.getParameterMap());
		Page<LiteUserInfo> page = new Page<LiteUserInfo>(pageNo, pageSize);
		IPage<LiteUserInfo> pageList = liteUserInfoService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 * 添加
	 *
	 * @param liteUserInfo
	 * @return
	 */
	@AutoLog(value = "微信用户信息表-添加")
	@ApiOperation(value="微信用户信息表-添加", notes="微信用户信息表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody LiteUserInfo liteUserInfo) {
		liteUserInfoService.save(liteUserInfo);
		return Result.ok("添加成功！");
	}
	
	/**
	 * 编辑
	 *
	 * @param liteUserInfo
	 * @return
	 */
	@AutoLog(value = "微信用户信息表-编辑")
	@ApiOperation(value="微信用户信息表-编辑", notes="微信用户信息表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody LiteUserInfo liteUserInfo) {
		liteUserInfoService.updateById(liteUserInfo);
		return Result.ok("编辑成功!");
	}
	
	/**
	 * 通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "微信用户信息表-通过id删除")
	@ApiOperation(value="微信用户信息表-通过id删除", notes="微信用户信息表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		liteUserInfoService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 * 批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "微信用户信息表-批量删除")
	@ApiOperation(value="微信用户信息表-批量删除", notes="微信用户信息表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.liteUserInfoService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "微信用户信息表-通过id查询")
	@ApiOperation(value="微信用户信息表-通过id查询", notes="微信用户信息表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		LiteUserInfo liteUserInfo = liteUserInfoService.getById(id);
		return Result.ok(liteUserInfo);
	}

  /**
   * 导出excel
   *
   * @param request
   * @param liteUserInfo
   */
  @RequestMapping(value = "/exportXls")
  public ModelAndView exportXls(HttpServletRequest request, LiteUserInfo liteUserInfo) {
      return super.exportXls(request, liteUserInfo, LiteUserInfo.class, "微信用户信息表");
  }

  /**
   * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
  public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      return super.importExcel(request, response, LiteUserInfo.class);
  }

}
